package com.test.services;

import com.test.model.Course;
import com.test.repository.CourseRepository;
import com.test.utils.FileUploadUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.Date;
import java.util.Objects;

@Service
@Slf4j
public class CourseServiceImpl implements CourseService {
    @Autowired
    CourseRepository courseRepository;

//    @Override
//    public Step getOrderedSteps(Course course, int stepNumber) {
//        log.info("Getting step number: " + stepNumber + " from course: " + course.getTitle());
//        Optional<Course> optCourse = courseRepository.findById(course.getId());
//
//        if(!optCourse.isPresent()) {
//            log.info(course.getTitle() + " <-- Does not exist ***ERROR***");
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Requested course does not exist");
//        } else {
//            if (optCourse.get().getOrderedSteps().get(stepNumber) != null) {
//                return optCourse.get().getOrderedSteps().get(stepNumber);
//            } else {
//                log.info("Step# " + stepNumber + " <-- Does not exist in " + course.getTitle() + " ***ERROR***");
//                throw new ResponseStatusException(HttpStatus.NOT_FOUND,"No step exists with that ID in this course");
//            }
//        }
//    }

    @Override
    public Course getCourseById(int id) {
        log.info("Trying to retrieve course by ID: " + id);
        return courseRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND,"No course found with ID: " + id));
    }

//    @Override
//    public HttpStatus addStepToCourse(int id, Step step, int stepNumber) {
//        Course course = getCourseById(id);
//
//        //add step to list of Steps
//        course.addStep(step);
//        log.info("Added step: " + step.getTitle() + " to " + course.getTitle());
//
//        return HttpStatus.OK;
//    }

    @Override
    public Course createCourse(Course course) {
        log.info("Creating course: " + course.getTitle());
        return courseRepository.save(course);
    }

    @Override
    public Iterable<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    @Override
    public HttpStatus deleteCourseById(int id) {
        if(courseRepository.existsById(id)){
            courseRepository.deleteById(id);
        } else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"No course with that ID was found, Delete failed.");
        }

        return HttpStatus.OK;
    }

    @Override
    public Course updateCourse(int id, Course course) {
        Course originalCourse = courseRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND,"No course with that ID was found, Delete failed."));
        if (course.getId() != id) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE,"ID of Course does not match updated course");
        }

        return courseRepository.save(course);
    }

    @Override
    public HttpStatus addImgToCourse(int id, MultipartFile multipartFile) throws IOException {
        //safe retrieve course by id
        Course course = getCourseById(id);

        //get filename and set image path on course
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(multipartFile.getOriginalFilename()));
        course.setImagePath(fileName);

        Course savedCourse = courseRepository.save(course);

        //create timestamp and save as unique filename
        String timestamp = Long.toString(new Date().getTime());
        String uploadDir = "course-img/" + savedCourse.getId() + "_" + timestamp;
        FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);

        return HttpStatus.OK;
    }
}
