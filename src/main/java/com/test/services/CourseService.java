package com.test.services;

import com.test.model.Course;
import com.test.model.Step;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface CourseService {
//    public Step getStepFromCourseByStepNumber(Course course, int stepNumber);
    public Course getCourseById(int id);
    //public HttpStatus addStepToCourse(int courseId, Step step, int stepNumber);
    public Course createCourse(Course course);
    public Iterable<Course> getAllCourses();
    public HttpStatus deleteCourseById(int id);
    public Course updateCourse(int id, Course course);
    public HttpStatus addImgToCourse(int id, MultipartFile multipartFile) throws IOException;

}
