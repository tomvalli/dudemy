package com.test.controller;


import com.test.model.Course;
import com.test.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Objects;


@RestController
@RequestMapping(path = "/api")
public class CourseController {

    @Autowired
    CourseService courseService;

    @GetMapping("/")
    public ResponseEntity<Iterable<Course>> getAllCourses() {
        return new ResponseEntity<>(courseService.getAllCourses(), HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<Course> createCourse(@RequestBody Course course) {
        return new ResponseEntity<>(courseService.createCourse(course),HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Course> updateCourse(@PathVariable int id, @RequestBody Course course) {
        return new ResponseEntity<>(courseService.updateCourse(id, course), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Course> getCourseById(@PathVariable int id) {
        return new ResponseEntity<>(courseService.getCourseById(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteCourseById(@PathVariable int id) {
        return new ResponseEntity<>(courseService.deleteCourseById(id), HttpStatus.OK);
    }

    @PostMapping("/img")
    public ResponseEntity<HttpStatus> addImgToCourse(@PathVariable int id, @RequestParam("image") MultipartFile multipartFile) throws IOException {
        return new ResponseEntity<>(courseService.addImgToCourse(id, multipartFile), HttpStatus.OK);
    }

}
