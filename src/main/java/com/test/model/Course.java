package com.test.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="course")
@AllArgsConstructor
@NoArgsConstructor
public class Course {

    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @Getter
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "course", cascade = CascadeType.ALL, orphanRemoval = true)
    List<Step> steps;

    @Getter
    @Setter
    String title;

    @Getter
    @Setter
    String description;

    @Setter
    String imagePath;

    public Course(String title) {
        this.title = title;
        steps.add(new Step());
    }

    public void addStep(Step step) {
        this.steps.add(step);
    }

    public String getImagePath() {
        if(imagePath == null || imagePath.isEmpty()) {
            imagePath = "dudemy_temp.png";
        }
        return imagePath;
    }
}
